# UI Suite Material

A site-builder friendly [Material Design Web](https://m2.material.io/develop/web) theme for Drupal 10 and 11, using the [UI Suite](https://www.drupal.org/project/ui_suite).

Material is a brand-agnostic design system created by Google to help teams build high-quality digital experiences.

Use Material Design directly from Drupal display builders (layout builder, manage display, views, blocks layout...).

# Installation

This theme requires the following modules:

- [UI Patterns](https://www.drupal.org/project/ui_patterns) 2.x for the [UI components](https://m2.material.io/components)
- [UI Styles](https://www.drupal.org/project/ui_styles) for the utilities (typography, color, elevation...)

And suggests the following modules:

- [UI Skins](https://www.drupal.org/project/ui_skins) for the CSS Variables (colors, shape, grid..)
- [UI Examples](https://www.drupal.org/project/ui_examples) to browse the example page(s)

# Asset libraries

By default, Material Design Web is loaded from https://unpkg.com/material-components-web/ . You can override the library definition if you prefer to use a local copy.
